import React, { Component } from 'react';
import Layout from './components/Layout/layout'
import NoteContainerComponent from './containers/Notes/noteContainerComponent';

class App extends Component {
  render() {
    return (
      <div>
      <Layout>
        <NoteContainerComponent />
      </Layout>
      </div>
    );
  }
}

export default App;
