import React, { Fragment } from 'react';
import classes from './createComponent.css';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';


const createComponent = (props) => {
    return (
        <Fragment>
            <Typography variant="display1" gutterBottom>
                NOTES
            </Typography>
            <Button variant="contained" size="large" color="primary">
                CREATE
            </Button>
        </Fragment>
    )
}

export default createComponent;
