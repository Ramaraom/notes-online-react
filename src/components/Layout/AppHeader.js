import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import WithStyles from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import classes from './AppHeader.css';


const styles = {
    root:{
        flexgrow:1
    },
    grow:{
        flexgrow:1
    }
};


class AppHeader extends React.Component{

    render(){
        

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <ToolBar>
                        <IconButton color="inherit">
                            <MenuIcon />
                        </IconButton>
                        <Typography className={classes.grow} color="inherit">
                            Notes Online
                        </Typography>
                        <div>
                            <IconButton color="inherit">
                                <AccountCircle />
                            </IconButton>
                            
                        </div>
                    </ToolBar>
                </AppBar>
            </div>
        )
    }
}

AppHeader.propTypes = {
    //classes: PropTypes.object.isRequired
}

export default AppHeader