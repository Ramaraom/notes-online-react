import React, {Fragment} from 'react'
import AppHeader from './AppHeader';
const Layout = (props) => {
    return (
        <Fragment>
            <AppHeader></AppHeader>
            {props.children}
        </Fragment>
    )
}

export default Layout;