import React,{Fragment} from 'react';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import classes from './searchComponent.css';
import Search from '@material-ui/icons/Search'


const searchComponent = (props) => {
    return (
        <Fragment>
        <FormControl className={classes.textField}>
          <InputLabel htmlFor="adornment-search">Search</InputLabel>
          <Input
            id="adornment-search"
            type='text'
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="Toggle password visibility"
                  onClick={this.handleClickShowPassword}
                >
                  <Search />
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        {/* <Button variant="contained" size="large" color="primary">
                Search
        </Button> */}
        </Fragment>
    )
}

export default searchComponent;