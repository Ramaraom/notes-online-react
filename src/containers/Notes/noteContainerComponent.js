import React from 'react';
import CreateComponent from '../../components/Create/createComponent';
import SearchComponent from '../../components/Search/searchComponent';
import NoteGridComponent from '../../components/NoteGrid/noteGridComponent';
import classes from './noteContainerComponent.css';

class NoteContainerComponent extends React.Component{
    state = {
        
        notes:[]
    }
    render(){
        return (
            <div className={classes.container}>
                <div className={classes.createContainer}>
                    <div  className = {classes.createItem}><CreateComponent /></div>
                    <div className = {classes.searchItem}><SearchComponent  /></div>
                </div>
                
                <NoteGridComponent />
                
            </div>
        )
    }
}

export default NoteContainerComponent;